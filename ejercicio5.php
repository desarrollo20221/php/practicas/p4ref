<?php

$a = [
    "Lunes" => 100,
    "Martes" => 150,
    "Miercoles" => 300,
    "Jueves" => 20,
    "Viernes" => 50,
];

$b = "Miercoles";

// imprime "no se"
// los datos introducidos en la funcion array_key_exists() estan cambiados
if (array_key_exists($a, $b)) {
    echo $a[$b];
} else {
    echo "no se";
}
